import React, { useEffect } from 'react';
import classes from './Main.module.css';
import FeelingCard from '../components/Feeling Card/feelingCard'
import ListItems from '../components/ListItems/ListItems';
import { connect } from 'react-redux';
import * as actions from '../store/Actions/index';

const Main = props => {

useEffect(()=>{
    // creating the initial date which its information is going to be presented in the feeling card 
    var moment = require('moment');
    moment().format();
    var today = moment()
      .format('YYYY-MM-DD');

     props.onUpdatePresentationFeelingCard(today);

},[])
   
       
const backgroundsColors = [
    '180deg, #B0B4BA 0%, #C6C6C8 34.03%, #CDC9C9 44.97%, #CCC1C1 62.15%, #C1B7BB 74.13%, #ABADB7 86.11%, #9AA4AC 99.13%',
    'linear-gradient(180deg, #35394A 0%, #3B4057 47.16%, #1F202D 100%)',
    'linear-gradient(180deg, #5563A9 0%, #5B669B 44.56%, #242B4C 100%)',
    'linear-gradient(180deg, #7184E7 0%, #9CAFF6 49.77%, #8897C9 100%)',
    'linear-gradient(180deg, #9984D5 0%, #E39DB2 49.25%, #E687A4 100%)',
    'linear-gradient(180deg, #F57694 0%, #F7B189 62.67%, #FFD28F 99.65%)'
]

return(
    
    <section className={classes.main} style={{background: backgroundsColors[props.backgroundColor] }}>
        <header className={classes.header}></header>
        {/* <main className={classes.m}> */}
                <FeelingCard />
                <ListItems />
         
            
        {/* </main>
        <footer className={classes.footer}><p>July 2020</p></footer> */}
      
    </section>
)
         
    

}


const mapStateToProps = state => {
    return {
        Records: state.reducer.Records,
       backgroundColor : state.reducer.backgrounds,
       presentation : state.reducer.PresentingDate
    }
};

const mapDispatchToProps = dispatch => {
    return {
        // onUpdateRecords: (FormData) => dispatch(actions.UpdateRecords(FormData)),
        // onInitPresentingDate: (today) => dispatch(actions.InitPresentingDate(today))
        onUpdatePresentationFeelingCard: (date) => dispatch(actions.UpdatePresentationDate(date))
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Main) ;
import * as actionTypes from '../Actions/ActionTypes';
import { updateObject } from './utility';





const initialState = {
    Records: [
         { 
            feelingRate : '1',
            time : '2020-09-05' ,
            message : 'I told mom that what she said hurt my feelings and I told mom that what she said hurt my feelings and',
            id : '2020-09-05',
            timestamped : '2020-09-05'
          },
          { 
            feelingRate : '2',
            time : '2020-09-04' ,
            message : 'Some designer decided to use a custom input control that updates multiple elements on the screen with different colors—just because he thought attaching a sky metaphor to the UI would be cool. Hate to break it to you buddy, but the sky is for airplanes, clouds, and double-rainbows!',
            id : '2020-09-04',
            timestamped : '2020-09-04'
          },
          { 
            feelingRate : '3',
            time : '2020-09-03' ,
            message : 'feeling not bad',
            id : '2020-09-03',
            timestamped : '2020-09-03'
          },
          { 
            feelingRate : '4',
            time : '2020-09-02' ,
            message : 'im ok',
            id : '2020-09-02',
            timestamped : '2020-09-02'
          },
          { 
            feelingRate : '5',
            time : '2020-09-01' ,
            message : 'When I was tucking my daughter in for the night, she said unprompted, “Daddy, you’re the best!',
            id : '2020-09-01',
            timestamped : '2020-09-01'
          },
    ],
    PresentingDate : {

      feelingRate : '',
      time : '' ,
      message : '',
      id : '',
      timestamped : ''
    }
      
      
    ,

    backgrounds : '0'
    
};

// this function recieves a listItemId , if the list id exists it will update the record and if not it will add a new record to the array
const updateRecordsSuccess = ( state, action ) => {

   let newRecord = {...state}
   const ids = newRecord.Records.map((newRecord) => {
       return newRecord.id
   })
   if(ids.includes(action.orderId)){
    newRecord = newRecord.Records.filter((newRecord ) => {
        if( newRecord.id === action.orderId) {
          newRecord.feelingRate = action.orderData.feelingRate;
          newRecord.time = action.orderId;
          newRecord.message = action.orderData.message;
          newRecord.id = action.orderId;
          newRecord.timestamped = action.orderData.timestamped;
        }
      
              
             return newRecord
           
    } );
    return {
        ...state,
        Records : [...newRecord]
    }
   }else{
    const newRecord = updateObject( action.orderData, { timestamped: action.orderId } );
    return updateObject( state, {
       
        Records: state.Records.concat( newRecord )
    } );
   }
  
 

};

// this function update the presentingDate state which will be the listItem which is going to be presented in the feelingcard component
const initPresentingDateSuccess = (state , action) =>{

  let newPresentingDate = {...state}
 
  const record = newPresentingDate.Records.filter((newPresentingDate) => {
    
    if(newPresentingDate.id === action.Date){
    
      return newPresentingDate
    }
})
return updateObject( state, {
       
  PresentingDate: [...record]
} );

};
// this function updates the background color (main page) based on the feeling rate
const ChangeBackgroundColourSucces = (state , action) => {
  return updateObject(state , {
    backgrounds : action.RateCtlNum
  });
}

// this function updates which listitem is going to be presented in the feeling card component
const UpdatePresentationDateSucces = (state , action) =>{
  
  let newPresentingDate = {...state};
  let record =newPresentingDate.PresentingDate;
  const ids = newPresentingDate.Records.map((newPresentingDate) => {
    return newPresentingDate.id
})
if(ids.includes(action.Date)){
  
  record = newPresentingDate.Records.filter((record) => {
    
    if(record.id === action.Date){
    
      return record
    }
})

record = record[0]

}else{
  record =  { 
  feelingRate : '',
  time : action.Date,
  message : '',
  id : action.Date,
  timestamped : ''
}
}

return updateObject( state, {
       
  PresentingDate: record
} );
}
  
const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.UPDATE_RECORDS_SUCCESS: return updateRecordsSuccess( state, action )
        case actionTypes.INIT_PRESENTATION_DATE_SUCCESS: return initPresentingDateSuccess(state, action)
        case actionTypes.CHANGE_BACKGROUND_COLOR_SUCCESS: return ChangeBackgroundColourSucces(state, action)
        case actionTypes.UPDATE_PRESENTATION_DATE_SUCCESS: return UpdatePresentationDateSucces(state, action)
        default: return state;
    }
};

export default reducer;
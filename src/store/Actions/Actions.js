import * as actionTypes from './ActionTypes';

export const UpdateRecordsSuccess = ( id, orderData ) => {
    return {
        type: actionTypes.UPDATE_RECORDS_SUCCESS,
        orderId: id,
        orderData: orderData
    };
};
export const InitPresentingDateSuccess = (today) => {
    return {
        type: actionTypes.INIT_PRESENTATION_DATE_SUCCESS,
        today: today
    
    }
}
export const ChangeBackgroundColourSucces = (RateCtlNum) => {
    return {
        type: actionTypes.CHANGE_BACKGROUND_COLOR_SUCCESS,
        RateCtlNum: RateCtlNum
    
    }
}
export const UpdatePresentationDateSucces = (date , id) => {
    return {
        type: actionTypes.UPDATE_PRESENTATION_DATE_SUCCESS,
        Date: date,
        ID:id
    
    }
}
export const UpdateRecords = ( orderId , orderData ) => {
    return dispatch => {
        
                dispatch( UpdateRecordsSuccess( orderData , orderId ) );
            }
    };
    export const InitPresentingDate = ( today ) => {
        return dispatch => {
            
                    dispatch( InitPresentingDateSuccess( today) );
                }
        };
        export const ChangeBackgroundColour = (RateCtlNum) => {
            return dispatch => {
                dispatch(ChangeBackgroundColourSucces(RateCtlNum))
            }
        }
        export const UpdatePresentationDate = ((date , id) => {
            return dispatch => {
                dispatch(ChangeBackgroundColourSucces(id))
                dispatch(UpdatePresentationDateSucces(date , id))
            }
        })


import React , { useEffect , useState } from 'react';
import classes from './ListItems.module.css';
import ListItem from './ListItem/ListItem';
import { connect } from 'react-redux';
import * as actions from '../../store/Actions/index';



const ListItems = props => {

  
  const [dateArray , setDateArray] = useState([])
  let moment = require('moment');
  moment().format();

   useEffect(() => {
       //creating the array of 2020 dates to create a list 
      let moment = require('moment');
      moment().format();
   
        let tomorrow = moment().clone().add(1,'day').format('YYYY-MM-DD');
       
       let startDate= moment('2020-01-01').format('YYYY-MM-DD')
   
   
   let newDateArray = [];
   while(startDate !== tomorrow){
      newDateArray.unshift(startDate);
     startDate = moment(startDate).add(1, 'day').format('YYYY-MM-DD')
   }
   setDateArray(newDateArray)
    }, [])

  

    // maping into the 2020 days and create a list items for the all 2020 days
  let ListItems = dateArray.map((date, index) => (

<ListItem
  className={classes.ListItem}
  clicked={(date)=> props.onUpdatePresentationFeelingCard(date)} key={index} index={index + 1} Record={props.Records.filter(Record => {
   if(Record.id === date){
      return Record
   }
   return null
})} date={date }/> 

  ))
  
   
  
  
 
   return (
  <>
        
         <div className={classes.listItems}>
   
            {/* <ListItem/> */}
           {ListItems}
       

         </div>
         
         <time className={classes.footer}>{moment(props.PresentingDate.time).format('MMM')}</time>
      </>
   )
};





const mapStateToProps = state => {
   return {
      Records: state.reducer.Records,
      PresentingDate : state.reducer.PresentingDate
   }
 };
 
const mapDispatchToProps = dispatch => {
   return {
       onUpdatePresentationFeelingCard: (date, rate) => dispatch(actions.UpdatePresentationDate(date, rate)),
       
   };
 };
export default connect(mapStateToProps , mapDispatchToProps)(ListItems) ;




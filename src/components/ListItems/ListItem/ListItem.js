import React from 'react';
import classes from './ListItem.module.css';


const ListItem = (props)=>{

  
  let moment = require('moment');
  moment().format();
 

  let time = () =>{
    if (props.index === 2){
      time = 'yesterday'
    }else if(props.index === 1) {
       time = 'today'
    }else{
       time = moment(props.date).format('LL')
    }
    return time
  } 
   
  let listItemOpacityIndex = props.index < 7 ? props.index : 7
 



    return (

       
              <section onClick={()=>props.clicked(props.date)} className={classes.ListItem} style={{opacity: 1 - listItemOpacityIndex / 10 }}>

               <div className={classes.Rate}>{props.Record.length > 0 ? props.Record[0].feelingRate : ''}</div>
                <time className={classes.Date}>{
                  
                }
                
              
                
                {time()}</time>
    <p className={classes.Sms}>{props.Record.length > 0 ? props.Record[0].message.substring(0, 50) : ''}</p>
              </section>
       
 
       
          

    )
};

export default ListItem;




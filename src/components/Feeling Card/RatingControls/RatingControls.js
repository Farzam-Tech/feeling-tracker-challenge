import React , { Component} from 'react';
import classes from './RatingControls.module.css';
import { connect } from 'react-redux';


class RatingControls extends Component{



state = {
  rateRange : [1,2,3,4,5],

}


  render(){
    
    const inputClasses = [classes.RateControl];
    const colors=[null,'#353A4B','#5563AA','#7184E7', '#C788E6','#F7678A']
 
return (
  
  <div className={classes.RatingControls}>
        <p className={inputClasses.join('')}>Awful</p>
       {this.state.rateRange.map((rate, index) => {
       
              return <label value= {rate} key={index} className={inputClasses.join(' ')} style={ rate == this.props.backgroundColor ? {background : colors[this.props.backgroundColor ]} : null} 
              onClick={()=> this.props.clicked(rate)}
            >
              <input
                type="radio"
                value= {rate}
                className={classes.invisible}
                onClick={this.props.changed}
              />
             {rate}
            </label>
       })}
        
        <p className={classes.RateControl} >Amazing</p>
    
    </div>
);
}}
    




const mapStateToProps = state => {
  return {
      
     backgroundColor : state.reducer.backgrounds
  }
};




export default connect(mapStateToProps)(RatingControls) ;
import React, { useState , useEffect} from 'react';
import classes from './FeelingCard.module.css';
import RatingControls from './RatingControls/RatingControls';
import {connect} from 'react-redux';
import * as actions from '../../store/Actions/index';
import Button from  '../UI/Button/Button';


const FeelingCard = props => {

 const [recordData , setRecordData] = useState( {
        
        feelingRate : '',
        time : '' ,
        message : '',
        id : '',
        timestamped : ''
      });

      const [timeIsStamped , setTimeStamped] = useState(false);
      const [formIsValid , setFormIsValid] = useState(false);
      const[buttonText, setButtonText] = useState( {
        
        text : 'SAVE'
      });


      let moment = require('moment');
      moment().format();
    
      
     useEffect(() => {
     initState();
      setRecordData({...props.PresentingDate});
         changeBackgroundColour(props.PresentingDate.feelingRate);
      
           props.PresentingDate.timestamped.length > 0 ? setTimeStamped(true): setTimeStamped(false)
           setButtonText({text : props.PresentingDate.timestamped})
          
      
     },[props.PresentingDate ])
// reseting the state after each rerendering 
const initState= () => {
  const resetData = {
        
    feelingRate : '',
    time : '' ,
    message : '',
    id : '',
    timestamped : ''
  };
  setRecordData(resetData)
}

// this function checks if both the textarea and rating control feeling value have been touched by the user
const checkFormValidity = (updatedOrderForm) => {

  if( updatedOrderForm.feelingRate && updatedOrderForm.feelingRate.trim() !== '' && updatedOrderForm.message && updatedOrderForm.message.trim()!== '' ){
    setFormIsValid(true);
    setButtonText({text : 'SAVE'})
  }

}

 

const inputIdentifiers = {ratingControls : 'feelingRate' , textarea : 'message' , time : 'time' , id : 'time' };
const feelings ={ 1 : 'sad' , 2 : 'notbad' , 3 : 'soso' , 4: 'good' , 5 : 'perfect'}

// this function uodates the states based on the form input information by the user 
const inputChangedHandler = (event, inputIdentifier) => {

  let updatedOrderForm = {
     ...recordData
  };
  
  let updatedFormElement = { 
      ...updatedOrderForm[inputIdentifier]
  };
  updatedFormElement = event.target.value;

  updatedOrderForm[inputIdentifier] = updatedFormElement;

  checkFormValidity(updatedOrderForm);
  setRecordData({...updatedOrderForm });

}
// recieves rate and updates the background color based on that
const changeBackgroundColour = (RateControlNum) => {
  props.onChangeBackgroundColour(RateControlNum)

}
// handels the form input information 
const orderHandler = ( event ) => {
  event.preventDefault();

  let formData = {
    ...recordData
  };

  formData.timestamped = moment().format('YYYY-MM-DD')
      setRecordData(formData)
      setTimeStamped(true);

       setButtonText({text :  formData.timestamped })
       props.onUpdateRecords(formData , formData.id); 
        setFormIsValid(false)
  }
           
 return (
   
 
        <div 
        
        className={classes.FeelingCardMain}
        >
           
            <form 
            className={classes.FeelingCard }
            onSubmit={orderHandler}>
              
              <RatingControls 
              clicked={(RateControlNum) => changeBackgroundColour(RateControlNum)}
              changed={(event) => inputChangedHandler(event, inputIdentifiers['ratingControls'])}
              backgrounds= {props.backgrounds}
              btnColor = {recordData.feelingRate}
              />
              <textarea 
              className={classes.MessageArea} 
              onChange={(event) => inputChangedHandler(event, inputIdentifiers['textarea'])}
              placeholder={recordData.message === '' ? 'What made you feel that way?' : props.PresentingDate.message}
              value={recordData.message}/>


              <Button disabled={ !timeIsStamped && !formIsValid}  btnType={ feelings[recordData.feelingRate]} >
                 {buttonText.text}
                 </Button>

             <time
              onChange={(event) => inputChangedHandler(event, inputIdentifiers['time'])}
             className={classes.Time}>{moment(props.PresentingDate.time).format('LL') }</time>
          </form>
               
        </div>
          
    )

           };

const mapStateToProps = state => {
  return {
    Records: state.reducer.Records,
    PresentingDate : state.reducer.PresentingDate,
    backgroundColor : state.reducer.backgrounds
  }
};

const mapDispatchToProps = dispatch => {
  return {
      onUpdateRecords: (FormData , id) => dispatch(actions.UpdateRecords(FormData , id)),
      onChangeBackgroundColour : (RateControlNum) => dispatch(actions.ChangeBackgroundColour(RateControlNum) )
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(FeelingCard) ;
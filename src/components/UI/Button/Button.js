import React from 'react';

import classes from './Button.module.css';

const button = (props) => (
    <button
        disabled={props.disabled}
        className={[classes.save, classes[props.btnType], classes[props.visible]].join(' ')}
        onClick={props.clicked}>{props.children}</button>
);

export default button;
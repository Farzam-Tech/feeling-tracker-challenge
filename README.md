# Feeling-Tracker-Challenge

A single page application to track user's feeling everyday.  there is a list of small cards the bottom which shows user's feeling and a message to indicate why the user felt that way. the numbers from 1 (very sad) to 5 (perfect) indicates users feeling. by clicking on the small list item user can see the full information in the bigger screen (card) at the center and also user can edit the information by choosing the date.  

#Technologies

React JS 
Redux(managing the state)
Css Module(to make all css classes' name unique)
Css3,HTML,JS, for DOM
React-Redux-Thunk (apply middelware)
Moment library (for date and time calculations)

#Launch

1) Install node js package to get access to npm
2) run npm i to init the project dependencies
3)run nom start to start the project on local host 


#Components

1) Main (it used to be container in older versions): the main component works like a layout to wrap the application.
This component build the website structure and has two subdivision components. 

2) FeelingCard : this component build the big scrren card to show the full information about the user feeling mood ( 1: awful to 5 :amazing) and also a message about why user feel so. in this component user can rate his/her feeling mood through the controls component and also write a message in the text area, based on his feeling the background color will be adjasted.

3) listItems : This component generates small list items (blocks) with brief information about pervious data user has submitted. this component allow user to click on the desiered date and see the information in feeling card component, so information can be editted.


#Developing Status

Work in progress


#Developers

Farzam Ajili
Ryerson University




